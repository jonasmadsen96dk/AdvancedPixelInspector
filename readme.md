# AdvancedPixelInspector

Inspects Pixels With Option To Export them.

## Getting Started

Simply open in Intellij should work out of the box.
Note Java 1.8 (8) is used.

## Deployment

I have compiled an artifact .jar file for quickly testing the software.

## Authors

* **Jonas Madsen** - *Initial work* - [Jonasmadsen](https://gist.github.com/Jonasmadsen)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
